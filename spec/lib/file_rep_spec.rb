require_relative '../../lib/file_rep'

describe FileRep do
  subject { described_class.new }
  let(:input) { double( url_name: '/home', ip_adress_adress: '0.0.0.0') }

  describe '#add' do
    before { subject.add(input) }

    it 'adds a new element to the array' do
      expect(subject[input.url_name]).to eq [input]
    end

    context 'when ip_adress is listed more than once' do
      before { subject.add(input) }
      it 'it is added to array' do
        expect(subject[input.url_name]).to eq [input ,input]
      end
    end
  end



  describe '#sort' do
    let(:first) { double( url_name: 'alpha', ip_adress: '1') }
    let(:second) { double( url_name: 'beta', ip_adress: '2') }
    let(:third) { double( url_name: 'theta', ip_adress: '3') }

    before do
      subject.add(second)
      subject.add(third)
      subject.add(first)
    end

    it 'returns sorted' do
      expect(subject.sort { |k, _| k })
          .to eq [['theta', [third]], ['beta', [second]], ['alpha', [first]]]
    end
  end
end