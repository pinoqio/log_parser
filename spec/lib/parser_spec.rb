require_relative '../../lib/parsers/parser'

describe Parser do
it "requires an existing file to parse" do
  file = "-------"
  allow(File).to receive(:exists?).with(file).and_return(false)
  expect{ Parser.new(file) }.to raise_exception(ArgumentError)
  end
end