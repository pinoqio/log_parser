require_relative '../../lib/parsers/line_parser.rb'

describe LineParser do
  let(:url_name) { '/home' }
  let(:ip_adress) { '127.00.00' }

  subject { described_class.new(url_name, ip_adress) }

  describe '#initialize' do
    it 'set #url_name' do
      expect(subject.url_name).to eq url_name
    end

    it 'set #ip_adress' do
      expect(subject.ip_adress).to eq ip_adress
    end
  end

  describe '.parse' do
    let(:line) { '/home 1.1.1.1' }

    it 'it yields a line' do
      expect { |b| described_class.parse(line, &b) }.to yield_with_args(LineParser)
    end
  end
end