require_relative '../../lib/parsers/argument_parser'

describe ArgumentParser do

  describe '.parse' do
    context 'when no arguments provided' do
      let(:args) { [] }

      it 'it breaks and exit' do
        expect { described_class.parse(args) }.to raise_error SystemExit
      end
    end

    context 'when more than one argument provided' do
      let(:args) { ['filename', 'other argument'] }

      it 'it breaks and exit' do
        expect { described_class.parse(args) }.to raise_error SystemExit
      end
    end
  end
end