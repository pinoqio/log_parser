require 'forwardable'

class FileRep
  extend Forwardable

  def_delegators :@url_name, :[]
  def initialize
    @url_name = Hash.new { |h, k| h[k] = [] }
  end

  def add(input)
    @url_name[input.url_name] << input
  end

  def sort(&block)
    @url_name.sort_by(&block).reverse
  end
end