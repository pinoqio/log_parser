require_relative '../lib/file_rep'
require_relative '../lib/parsers/parser'


class Main
  def initialize(file, autorun = true)
    @file = FileRep.new
    @filename = file

    parse if autorun
  end

  def parse
    Parser.parse(@filename) { |entry| @file.add(entry) }
  end

  def visits
    @file.sort { |_, v| v.count }.each do |url_name, entries|
      format('visits', url_name, entries.count)
    end
  end

  def views
    @file.sort { |_, v| v.uniq(&:ip_adress).count }.each do |url_name, entries|
      format('unique views', url_name, entries.uniq(&:ip_adress).count)
    end
  end

  private

  def format( string, url, count)
    puts sprintf('| %-15s | %-15s | %-3d |', url, string, count)
  end
end



