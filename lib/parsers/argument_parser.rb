class ArgumentParser

  def self.parse(args)
    valid_arguments? unless args.length == 1
    return { filename: args[0] }
  end

  private
  def self.valid_arguments?
    exit(1)
  end

end
