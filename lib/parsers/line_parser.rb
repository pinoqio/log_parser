class LineParser
  attr_accessor :url_name, :ip_adress

  def initialize(url_name, ip_adress)
    @url_name = url_name
    @ip_adress =  ip_adress
  end

  def self.parse(line)
    line.split(' ').tap { |url_name, ip_adress| yield LineParser.new(url_name, ip_adress) }
  end
end

