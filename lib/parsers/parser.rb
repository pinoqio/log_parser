require_relative '../../lib/parsers/line_parser'

class Parser
  def self.parse(file, &block)
    fail "file #{file} not found" unless File.exist? file
    puts 'fds'
    File.open(file, 'r').each_line { |line| LineParser.parse(line, &block) }
  end
end
