require_relative '../lib/parsers/argument_parser'
require_relative '../lib/main'

class CliDisplay
  def self.parse_file
    file = ArgumentParser.parse(ARGV)[:filename]
    main = Main.new(file)
    puts ' ----- Visits ----------------------------'
    main.visits
    puts ' ----- Unique Views ----------------------'
    main.views
    puts ' -----------------------------------------'
  end

  parse_file
end